#!/usr/bin/bash

#############################################################
# DSSDDL
#############################################################
##=== DSSDDL-v1 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python dssddlv1.py --dataset=cifar10 --sup-batch-size=50 --usp-batch-size=50 --label-exclude=False --num-labels=4000 --arch=cnn13-feat --model=dssddlv1 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --optim=adam --epochs=400 --lr=1e-3 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=0 2>&1 | tee results/dssddlv1_cifar10-500_$(date +%y-%m-%d-%H-%M).txt
# cifar10-2k
#CUDA_VISIBLE_DEVICES=$1 python dssddlv1.py --dataset=cifar10 --sup-batch-size=50 --usp-batch-size=50 --label-exclude=False --num-labels=2000 --arch=cnn13-feat --model=dssddlv1 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --optim=adam --epochs=400 --lr=1e-3 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=0 2>&1 | tee results/dssddlv1_cifar10-500_$(date +%y-%m-%d-%H-%M).txt
# cifar10-1k
#CUDA_VISIBLE_DEVICES=$1 python dssddlv1.py --dataset=cifar10 --sup-batch-size=50 --usp-batch-size=50 --label-exclude=False --num-labels=1000 --arch=cnn13-feat --model=dssddlv1 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --optim=adam --epochs=400 --lr=1e-3 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=0 2>&1 | tee results/dssddlv1_cifar10-500_$(date +%y-%m-%d-%H-%M).txt
# cifar10-500
#CUDA_VISIBLE_DEVICES=$1 python dssddlv1.py --dataset=cifar10 --sup-batch-size=50 --usp-batch-size=50 --label-exclude=False --num-labels=500 --arch=cnn13-feat --model=dssddlv1 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --optim=adam --epochs=400 --lr=1e-3 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=0 2>&1 | tee results/dssddlv1_cifar10-500_$(date +%y-%m-%d-%H-%M).txt

# cifar10-4k wide-resnet
#CUDA_VISIBLE_DEVICES=$1 python dssddlv1.py --dataset=cifar10 --sup-batch-size=50 --usp-batch-size=50 --label-exclude=False --num-labels=4000 --arch=wide-feat --model=dssddlv1 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --optim=adam --epochs=400 --lr=1e-3 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=0 2>&1 | tee results/dssddlv1_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt

##=== MixUp DSSDDL-v1 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python dssddlv1.py --dataset=cifar10 --sup-batch-size=50 --usp-batch-size=50 --label-exclude=False --num-labels=4000 --arch=cnn13-feat --model=mixdssddlv1 --sdlde-weight=1.0 --sdlde-ent-th=1.2 --sdlde-ent-inc=0.2 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --mixup-alpha=1.0 --optim=adam --epochs=400 --lr=1e-3 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=0 2>&1 | tee results/mixdssddlv1_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt

##########################################################
#     DSSDDL-MeanTeacher
##########################################################
##=== DSSDDL-MeanTeacher-v1 ===
# cifar10-500
#CUDA_VISIBLE_DEVICES=$1 python dssddlv1.py --dataset=cifar10 --sup-batch-size=16 --usp-batch-size=112 --label-exclude=False --num-labels=500 --arch=cnn13-feat --model=mt-dssddlv1 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --usp-weight=100.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=exp-warmup --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --data-idxs=True --save-freq=0 2>&1 | tee results/dssddl-mtv1_cifar10-500_$(date +%y-%m-%d-%H-%M).txt

##=== DSSDDL-MeanTeacher-v2 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=4000 --arch=cnn13-feat --model=mt-dssddlv2 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --usp-weight=100.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --data-idxs=True --save-freq=100 2>&1 | tee results/dssddl-mtv2_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-2k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=2000 --arch=cnn13-feat --model=mt-dssddlv2 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --usp-weight=100.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --data-idxs=True --save-freq=100 2>&1 | tee results/dssddl-mtv2_cifar10-2k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-1k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=1000 --arch=cnn13-feat --model=mt-dssddlv2 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --usp-weight=100.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --data-idxs=True --save-freq=100 2>&1 | tee results/dssddl-mtv2_cifar10-1k_$(date +%y-%m-%d-%H-%M).txt

##=== DSSDDL-MeanTeacher-v2 WRN-28-2 ===
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=4000 --arch=wide-feat --model=mt-dssddlv2 --sdlde-weight=1.0 --sdlde-ent-th=0.6 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --usp-weight=100.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --data-idxs=True --save-freq=100 2>&1 | tee results/dssddl-mtv2_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt

##=== MixUp DSSDDL-MeanTeacher-v2 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=4000 --arch=cnn13-feat --model=mt-mixdssddlv2 --sdlde-weight=1.0 --sdlde-ent-th=1.2 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --usp-weight=30.0 --mixup-alpha=1.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=100 2>&1 | tee results/mixdssddl-mtv2_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-1k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=1000 --arch=cnn13-feat --model=mt-mixdssddlv2 --sdlde-weight=1.0 --sdlde-ent-th=1.2 --sdlde-beta=0.6 --sdlde-eta=0.9  --is-normalize=True --start-epoch=10 --sdlde-freq-n=11 --usp-weight=30.0 --mixup-alpha=1.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --data-idxs=True --save-freq=100 2>&1 | tee results/mixdssddl-mtv2_cifar10-1k_$(date +%y-%m-%d-%H-%M).txt

##########################################################
#     PI-ALC
##########################################################
##=== PI-ALC-v2 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=4000 --arch=cnn13-at --model=pi-alcv2 --alc-weight=1.0 --usp-weight=30.0 --drop-ratio=0.5 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --save-freq=100 2>&1 | tee results/pi-alcv2_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-2k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=2000 --arch=cnn13-at --model=pi-alcv2 --alc-weight=1.0 --usp-weight=30.0 --drop-ratio=0.5 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --save-freq=100 2>&1 | tee results/pi-alcv2_cifar10-2k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-1k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=1000 --arch=cnn13-at --model=pi-alcv2 --alc-weight=1.0 --usp-weight=30.0 --drop-ratio=0.5 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=exp-warmup --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --save-freq=100 2>&1 | tee results/pi-alcv2_cifar10-1k_$(date +%y-%m-%d-%H-%M).txt


##########################################################
#     MeanTeacher-ALC
##########################################################
##=== MeanTeacher-ALC-v2 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=4000 --arch=cnn13-at --model=mt-alcv2 --alc-weight=1.0 --usp-weight=30.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --save-freq=100 2>&1 | tee results/mt-alcv2_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-2k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=2000 --arch=cnn13-at --model=mt-alcv2 --alc-weight=1.0 --usp-weight=30.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --save-freq=100 2>&1 | tee results/mt-alcv2_cifar10-2k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-1k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=100 --label-exclude=False --num-labels=1000 --arch=cnn13-at --model=mt-alcv2 --alc-weight=1.0 --usp-weight=30.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=True --save-freq=100 2>&1 | tee results/mt-alcv2_cifar10-1k_$(date +%y-%m-%d-%H-%M).txt


##########################################################
#     SDB-Net
##########################################################
##=== SDB-Netv2 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=200 --label-exclude=False --num-labels=4000 --arch=cnn13-db --model=sdb-netv2 --usp-weight=30.0 --mixup-alpha=1.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --save-freq=100 2>&1 | tee results/sdbnetv2_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-2k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=200 --label-exclude=False --num-labels=2000 --arch=cnn13-db --model=sdb-netv2 --usp-weight=30.0 --mixup-alpha=0.9 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --save-freq=100 2>&1 | tee results/sdbnetv2_cifar10-2k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-1k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=200 --label-exclude=False --num-labels=1000 --arch=cnn13-db --model=sdb-netv2 --usp-weight=30.0 --mixup-alpha=0.5 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --save-freq=100 2>&1 | tee results/sdbnetv2_cifar10-1k_$(date +%y-%m-%d-%H-%M).txt

##=== SDB-Netv2 WRN-28-2 ===
# cifar10-4k
#CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=200 --label-exclude=False --num-labels=4000 --arch=wide-db --model=sdb-netv2 --usp-weight=30.0 --mixup-alpha=1.0 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --save-freq=100 2>&1 | tee results/sdbnetv2_wide_cifar10-4k_$(date +%y-%m-%d-%H-%M).txt
# cifar10-1k
CUDA_VISIBLE_DEVICES=$1 python papers.py --dataset=cifar10 --sup-batch-size=100 --usp-batch-size=200 --label-exclude=False --num-labels=1000 --arch=wide-db --model=sdb-netv2 --usp-weight=30.0 --mixup-alpha=0.5 --ema-decay=0.97 --optim=sgd --epochs=400 --lr=0.1 --momentum=0.9 --weight-decay=5e-4 --nesterov=True --lr-scheduler=cos --min-lr=1e-4 --rampup-length=80 --rampdown-length=50 --data-twice=False --save-freq=100 2>&1 | tee results/sdbnetv2_wide_cifar10-1k_$(date +%y-%m-%d-%H-%M).txt
