import torch
import numpy as np
from time import time
from torch.nn import functional as F

from utils.fddl import FDDL
from utils.datasets import decode_label
from utils.data_utils import NO_LABEL

from pdb import set_trace

DEBUG = True

fddl = FDDL()

softp = None

def sdlde_update(model, p, labeledData,Data, par, device):
    global softp
    print("sdlde_utilsv2")
    softp = p.clone().detach()
    if par['is_normalize']: print("using normalized features")
    dictionary, Dlabels = update_dict(model, labeledData, device,
                                      par['is_normalize'])
    compute_p(model, Data, dictionary, Dlabels, p, par, device)
    par['sdlde_step'] += 1

def update_dict(model, labeledData, device, is_normalize):
    features, labels = [], []
    print("update dictionary...");    t1 = time()
    for data, targets, idxs in labeledData:
        if isinstance(data, list): data = data[0]
        data, targets = data.to(device), targets.to(device)
        _, feats = model(data)
        features.append(feats.detach())
        labels.append(targets)
    features = torch.cat(features, dim=0)
    if is_normalize:
        features = normalize(features)
    labels = torch.cat(labels)
    Dlabels, sorted_idx = torch.sort(labels)
    t2 = time();     print(f"update costs {t2-t1:.5f}s")
    return features.index_select(dim=0, index=sorted_idx), Dlabels

def compute_err(dictionary, feats, Dlabels,n_class, kappa=0.001):
    error = np.zeros((feats.shape[0], n_class))
    coefMat = fddl.fddl(dictionary.T, feats.T)
    for j in range(n_class):
        coef_c = coefMat[Dlabels==j]
        Dc = dictionary[Dlabels==j].T
        err_c = feats.T-np.dot(Dc, coef_c)
        error[:,j]  = np.linalg.norm(err_c, 2, axis=0)**2
        error[:,j] += kappa* np.linalg.norm(coef_c, 1, axis=0)
    return torch.FloatTensor(error)

def compute_p(model,train_data, dictionary,Dlabels, pslab, par, device):
    global softp
    print("compute p ...")
    for batch_idx, (data, targets, idxs) in enumerate(train_data):
        if isinstance(data, list): data = data[0]
        t1 = time()
        data, targets = data.to(device), targets.to(device)
        outputs_net, feats = model(data)
        # check the softmax output
        softp[idxs] = outputs_net.max(1)[1]
        if par['is_normalize']: feats = normalize(feats)
        err = compute_err(dictionary.cpu().numpy(),
                          feats.detach().cpu().numpy(),
                          Dlabels.cpu().numpy(),
                          par['nClass']).to(device)
        #err = err.to(device)
        omiga  = par['eta']*err
        omiga -= (1-par['eta'])*F.log_softmax(outputs_net.detach(), dim=1)
        p_temp = F.softmax(-1.0* omiga / par['beta'], dim=1)
        if DEBUG and (batch_idx%100)==0:
            log_p(p_temp, outputs_net.detach())
        batch_pslab, lam = select_p(p_temp,
                               par['prob'], par['prob_dec'],
                               par['ent'], par['ent_inc'],
                               par['sdlde_step'])
        if DEBUG and (batch_idx%100)==0:
            print('ent: {}, inc: {}, lam: {}'.format(par['ent'], par['ent_inc'], lam))
        usel_mask = batch_pslab.eq(NO_LABEL)
        batch_pslab[usel_mask] = pslab[idxs][usel_mask]
        pslab[idxs] = batch_pslab

        if batch_idx % 50 == 0:
            t2 = time(); print(f"SDLDE[{batch_idx:<3}]\t costs {t2-t1:.5f}s")

def select_p(p_temp, prob_max, prob_dec, lam_min, lam_inc, sdlde_step):
    val, pred = p_temp.max(1)
    entropy = torch.sum(-1.0* p_temp* torch.log(p_temp+1e-10), dim=1)
    prob = prob_max - sdlde_step*prob_dec
    lam = lam_min + sdlde_step*lam_inc
    lam = min(lam, 2.2)
    #pred[val<prob] = NO_LABEL
    pred[entropy>lam] = NO_LABEL
    return pred, lam

def validate_p(train_data, pslab, device):
    correct_n, select_n = 0, 0
    correct_soft, select_soft = 0, 0
    for batch_idx, (_, targets, idxs) in enumerate(train_data):
        # check dssddl
        batch_pslab, targets = pslab[idxs], targets.to(device)
        umask = targets.le(NO_LABEL)
        ul_targets, ul_p = decode_label(targets[umask]), batch_pslab[umask]
        select_mask = ul_p.ne(NO_LABEL)
        correct_n += ul_targets[select_mask].eq(ul_p[select_mask]).float().sum()
        select_n  += select_mask.float().sum()
        # check softmax outputs
        batch_soft = softp[idxs]
        correct_soft += ul_targets.eq(batch_soft[umask]).float().sum()
        select_soft += umask.float().sum()
    print('==> the accuracy of pslab is {:.4f}%, update num is {}'.format(
          correct_n/(select_n+1e-10), select_n))
    print('==> the accuracy of softp is {:.4f}%, update num is {}'.format(
          correct_soft/(select_soft+1e-10), select_soft))
    return correct_n/(select_n+1e-10)

def normalize(x):
    return F.normalize(x, p=2, dim=1)

def log_p(p_temp, outputs_net):
    val, pred = p_temp.max(1)
    entropy = torch.sum(-1.0* p_temp* torch.log(p_temp+1e-10), dim=1)
    print('softmax outputs')
    soft_net = F.softmax(outputs_net, 1)
    print(soft_net[:2])
    print(soft_net.max(1)[0][:10])
    print('dssddl outputs')
    print(p_temp[:2])
    print(p_temp.max(1)[0][:10])
    print('entropy: ', torch.mean(entropy))
    print('max val: ', torch.mean(val))

