# -*- coding: utf-8 -*-
"""
@author: CJM
"""
import numpy as np
from scipy.sparse.linalg import eigs

class FDDL():
    
    def __init__(self):
        self.kapp = 0.001

    def fddl(self, dictMat, dataMat):
        '''
        dataMat.shape(n_feat, n_sample)
        dictMat.shape(n_feat, n_atom)
        coefMat.shape(n_atom, n_sample)
        '''
        # initialization
        coefMat = np.zeros((dictMat.shape[1], dataMat.shape[1]))
        
        # compute grad
        #DD = np.dot(dictMat.T, dictMat)
        #grad = 2*np.dot(DD, coefMat) - 2*np.dot(dictMat.T, dataMat)
        grad = -2.0*np.dot(dictMat.T, dataMat)
        
        # sigma
        if dictMat.shape[0] > dictMat.shape[1]:
            val, _ = eigs(np.dot(dictMat.T, dictMat), k=1)
            sigma = 1.05*val
        else:
            val, _ = eigs(np.dot(dictMat, dictMat.T), k=1)
            sigma = 1.05*val
        
        # update coef once
        coefMat = coefMat - grad/(2*sigma)
        tau = self.kapp/2
        return self.soft(coefMat, tau/sigma)
    
    def soft(self, x, tau):
        left = np.sign(x)
        right = np.clip( np.abs(x)-tau, a_min=0, a_max=None)
        return np.multiply(left, right)
