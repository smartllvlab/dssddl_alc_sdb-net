#!coding:utf-8
import torch
from torch.nn import functional as F

import os
import datetime, time
from pathlib import Path
from collections import defaultdict

from utils.loss import one_hot
from utils.mixup import *
from utils.ramps import exp_rampup
from utils.data_utils import NO_LABEL
#from utils.sdlde_utils import sdlde_update, validate_p
from utils.sdlde_utilsv2 import sdlde_update, validate_p

from pdb import set_trace

class Trainer:

    def __init__(self, model, optimizer, device, config):
        print('Mixup DSSDDL-v1')
        self.model     = model
        self.optimizer = optimizer
        self.ce_loss   = torch.nn.CrossEntropyLoss(ignore_index=NO_LABEL)
        self.save_dir  = '{}-{}_{}-{}_{}'.format(config.arch, config.model,
                          config.dataset, config.num_labels,
                          datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_dir  = os.path.join(config.save_dir, self.save_dir)
        self.save_freq   = config.save_freq
        self.print_freq  = config.print_freq
        self.device      = device
        self.epoch       = 0
        self.alpha       = config.mixup_alpha

        self.start_epoch  = config.start_epoch
        self.sdlde_weight = config.sdlde_weight
        self.sdlde_freq_n = config.sdlde_freq_n+1
        self.sdlde_freq = config.epochs // self.sdlde_freq_n
        if config.lr_scheduler=='exp-warmup':
            self.start_epoch = self.sdlde_freq
        self.par = {'eta':config.sdlde_eta,      'beta':config.sdlde_beta,
                    'prob':config.sdlde_prob_th, 'prob_dec':config.sdlde_prob_dec,
                    'ent':config.sdlde_ent_th,   'ent_inc':config.sdlde_ent_inc,
                    'sdlde_step': 0,             'is_normalize': config.is_normalize}
        self.rampup = exp_rampup(self.sdlde_freq)
        #self.rampup = exp_rampup(config.rampup_length)
        
    def train_iteration(self, data_loader, print_freq):
        loop_info = defaultdict(list)
        labeled_n, unlabeled_n = 0, 0
        for batch_idx, (data, targets, idxs) in enumerate(data_loader):
            if isinstance(data, list): data = data[0]
            data, targets = data.to(self.device), targets.to(self.device)
            ##=== decode targets ===
            lmask, umask = self.decode_targets(targets)
            lbs, ubs = lmask.float().sum().item(), umask.float().sum().item()

            ##=== forward ===
            outputs, _ = self.model(data)
            loss = self.ce_loss(outputs[lmask], targets[lmask])
            loop_info['lloss'].append(loss.item())

            ##=== Training Phase ===
            if self.epoch>self.start_epoch:
                # dssddl
                iter_unlab_pslab = self.epoch_pslab[idxs]
                uloss  = self.ce_loss(outputs[umask], iter_unlab_pslab[umask])
                uloss *= self.rampup(self.epoch)*self.sdlde_weight
                loss  += uloss; loop_info['uloss'].append(uloss.item())
                # mixup
                select_mask = iter_unlab_pslab.ne(NO_LABEL)
                if select_mask.sum()>1:
                   #soft_pslab = one_hot(iter_unlab_pslab[select_mask])
                   mixed_x, ya, yb, lam = mixup_two_targets(data[select_mask],
                                                iter_unlab_pslab[select_mask],
                                                      self.alpha, self.device)
                   mixed_outputs,_ = self.model(mixed_x)
                   mloss = mixup_ce_loss_hard(mixed_outputs, ya, yb, lam)
                   mloss *= self.rampup(self.epoch)*self.usp_weight
                   loss += mloss; loop_info['umix'].append(mloss.item())
                else:
                   loop_info['umix'].append(0)

            ##=== bachward 
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            ##=== log info ===
            labeled_n, unlabeled_n = labeled_n+lbs, unlabeled_n+ubs
            lacc = targets[lmask].eq(outputs[lmask].max(1)[1]).float().sum().item()
            uacc = targets[umask].eq(outputs[umask].max(1)[1]).float().sum().item()
            loop_info['lacc'].append(lacc)
            loop_info['uacc'].append(uacc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[train][{batch_idx:<3}]", self.gen_info(loop_info, lbs, ubs))
        print(f">>>[train]", self.gen_info(loop_info, labeled_n, unlabeled_n, False))
        return loop_info, labeled_n

    def test_iteration(self, data_loader, print_freq):
        loop_info = defaultdict(list)
        labeled_n, unlabeled_n = 0, 0
        for batch_idx, (data, targets) in enumerate(data_loader):
            data, targets = data.to(self.device), targets.to(self.device)
            ##=== decode targets ===
            lmask = umask = targets.ge(0)
            lbs, ubs = lmask.float().sum().item(), umask.float().sum().item()

            ##=== forward ===
            outputs, _ = self.model(data)
            loss = self.ce_loss(outputs[lmask], targets[lmask])
            loop_info['lloss'].append(loss.item())

            ##=== log info ===
            labeled_n += lbs
            unlabeled_n += ubs
            lacc = targets[lmask].eq(outputs[lmask].max(1)[1]).float().sum().item()
            loop_info['lacc'].append(lacc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[test][{batch_idx:<3}]", self.gen_info(loop_info, lbs, ubs))
        print(f">>>[test]", self.gen_info(loop_info, labeled_n, unlabeled_n, False))
        return loop_info, labeled_n
        
    def train(self, data_loader, print_freq=20):
        self.model.train()
        with torch.enable_grad():
            return self._iteration(data_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        with torch.no_grad():
            return self.test_iteration(data_loader, print_freq)

    def train_sdlde(self, labeled_data, train_data):
        self.model.eval()
        with torch.no_grad():
            print("====> sdlde update start <===")
            t1 = time.time()
            sdlde_update(self.model, self.epoch_pslab,
                         labeled_data, train_data,
                         self.par, self.device)
            t2 = time.time()
            print('===> sdlde complete, costs {}s'.format(t2-t1))
            print(self.epoch_pslab)
        validate_p(train_data, self.epoch_pslab, self.device)

    def loop(self, epochs, labeled_data, train_data, test_data, scheduler=None):
        ## construct epoch pseudo labels
        n_classes = train_data.dataset.num_classes
        self.epoch_pslab = self.create_pslab(n_samples=len(train_data.dataset),
                                             n_classes=n_classes,
                                             dtype='zero')
        self.epoch_pslab.fill_(NO_LABEL)
        self.par['nClass'] = n_classes
        ## main process
        best_info, best_acc, update_count, n = None, 0., 0, 0
        for ep in range(epochs):
            t0 = time.time()
            self.epoch = ep
            if scheduler is not None: scheduler.step()
            print("------ Training epochs: {} ------".format(ep))
            self.train(train_data, self.print_freq)

            ## sdlde update
            if ep>=self.start_epoch and ((ep-self.start_epoch)%self.sdlde_freq==0):
                #and (update_count<self.sdlde_freq_n) and self.sdlde_used:
                update_count += 1
                self.train_sdlde(labeled_data, train_data)

            print("------ Testing epochs: {} ------".format(ep))
            info, n = self.test(test_data, self.print_freq)
            tacc    = sum(info['lacc'])/n
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep, tacc)
            if self.save_freq!=0 and tacc>best_acc and (ep+1)>(epochs-50):
                self.save(ep, tacc, True)
            if tacc>best_acc: best_acc, best_info = tacc, info
            print('===== [epoch time]: {}s ====='.format(time.time()-t0))
        print(">>>[best]", self.gen_info(best_info, n, n, False))

    def create_pslab(self, n_samples, n_classes, dtype='rand'):
        if dtype=='rand': 
             pslab = torch.randint(0, n_classes, (n_samples,))
        elif dtype=='zero':
             pslab = torch.zeros(n_samples)
        else:
             raise ValueError('Unknown pslab dtype: {}'.format(dtype))
        return pslab.long().to(self.device)

    def decode_targets(self, targets):
        labeled_mask = targets.ge(0)
        unlabeled_mask = targets.le(NO_LABEL)
        targets[unlabeled_mask] = NO_LABEL*targets[unlabeled_mask]-1
        return labeled_mask, unlabeled_mask

    def gen_info(self, info, lbs, ubs, iteration=True):
        ret = []
        nums = {'l': lbs, 'u':ubs, 'a': lbs+ubs}
        for k, val in info.items():
            n = nums[k[0]]
            v = val[-1] if iteration else sum(val)
            s = f'{k}: {v/n:.3%}' if k[-1]=='c' else f'{k}: {v:.5f}'
            ret.append(s)
        return '\t'.join(ret)

    def save(self, epoch, acc, best=False):
        if best: epoch = 'best'
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                       "acc": acc,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
