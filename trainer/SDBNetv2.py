#!coding:utf-8
import torch
from torch.nn import functional as F

import os
import datetime
from pathlib import Path
from collections import defaultdict
from itertools import cycle

from utils.loss import mse_with_softmax
from utils.loss import softmax_loss_mean
from utils.mixup import *
from utils.ramps import exp_rampup
from utils.datasets import decode_label
from utils.data_utils import NO_LABEL

from pdb import set_trace

class Trainer:

    def __init__(self, model, ema_model, optimizer, device, config):
        print("SDB-Net-v2")
        self.model      = model
        self.ema_model  = ema_model
        self.optimizer  = optimizer
        self.ce_loss    = torch.nn.CrossEntropyLoss(ignore_index=NO_LABEL)
        self.mix_ce_loss = mixup_ce_loss_hard
        self.mixup_loss  = mixup_ce_loss_with_softmax #mixup_mse_loss_with_softmax
        self.save_dir  = '{}-{}_{}-{}_{}'.format(config.arch, config.model,
                          config.dataset, config.num_labels,
                          datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_dir  = os.path.join(config.save_dir, self.save_dir)
        self.global_step = 0
        self.epoch       = 0
        self.alpha       = config.mixup_alpha
        self.usp_weight  = config.usp_weight
        self.ema_decay   = config.ema_decay
        self.rampup      = exp_rampup(config.weight_rampup)
        self.device      = device
        self.save_freq   = config.save_freq
        self.print_freq  = config.print_freq

    def train_iteration(self, label_loader, unlab_loader, print_freq):
        loop_info = defaultdict(list)
        batch_idx, label_n, unlab_n = 0, 0, 0
        for (label_x, label_y), (unlab_x, unlab_y) in zip(cycle(label_loader), unlab_loader):
            self.global_step += 1; batch_idx+=1;
            label_x, label_y = label_x.to(self.device), label_y.to(self.device)
            unlab_x, unlab_y = unlab_x.to(self.device), unlab_y.to(self.device)
            ##=== decode targets of unlabeled data ===
            self.decode_targets(unlab_y)
            lbs, ubs = label_x.size(0), unlab_x.size(0)

            ##=== forward ===
            mixed_lx, ly_a, ly_b, llam = mixup_two_targets(label_x, label_y, 
                                    min(self.alpha/2,.2), self.device, True)
            mixed_outputs,_ = self.model(mixed_lx)
            loss = self.mix_ce_loss(mixed_outputs, ly_a, ly_b, llam)
            loop_info['lmixSup'].append(loss.item())

            ##=== Semi-supervised Training ===
            ## update mean-teacher
            self.update_ema(self.model, self.ema_model, self.ema_decay, self.global_step)
            with torch.no_grad():
                ema_outputs_u1, ema_outputs_u2 = self.ema_model(unlab_x)
                ema_outputs_u1 = ema_outputs_u1.detach()

            # mixup-consistency-v1
            mixed_ux, mixed_uy, lam = mixup_one_target(unlab_x, ema_outputs_u1,
                                                       self.alpha, self.device)
            mixed_outputs_u1, mixed_outputs_u2 = self.model(mixed_ux)

            mix_loss1  = mse_with_softmax(mixed_outputs_u1, mixed_uy)
            mix_loss1 *= self.rampup(self.epoch)*self.usp_weight
            loss += mix_loss1; loop_info['uMix1'].append(mix_loss1.item())

            mix_loss2  = mse_with_softmax(mixed_outputs_u2, mixed_uy)
            mix_loss2 *= self.rampup(self.epoch)*self.usp_weight
            loss += mix_loss2; loop_info['uMix2'].append(mix_loss2.item())

            # mixup-consistency-v2
            #mixed_ux, uy_a, uy_b, lam = mixup_two_targets(unlab_x, ema_outputs_u1, 
            #                               self.alpha, self.device, is_bias=False)
            #mixed_outputs_u1, mixed_outputs_u2 = self.model(mixed_ux)

            #mix_loss1  = self.mixup_loss(mixed_outputs_u1, uy_a, uy_b, lam)
            #mix_loss1 *= self.rampup(self.epoch)*self.usp_weight
            #loss += mix_loss1; loop_info['uMix1'].append(mix_loss1.item())

            #mix_loss2  = self.mixup_loss(mixed_outputs_u2, uy_a, uy_b, lam)
            #mix_loss2 *= self.rampup(self.epoch)
            #loss += mix_loss2; loop_info['uMix2'].append(mix_loss2.item())

            # co-consistency loss
            unlab_outputs1, unlab_outputs2 = self.model(unlab_x)
            with torch.no_grad():
                unlab_outputs1 = unlab_outputs1.detach()
            co_loss  = mse_with_softmax(unlab_outputs2, unlab_outputs1)
            co_loss *= self.rampup(self.epoch)
            loss += co_loss; loop_info['uCo'].append(co_loss.item())

            ## backwark
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            ##=== log info ===
            label_n, unlab_n = label_n+lbs, unlab_n+ubs
            loop_info['uacc'].append(unlab_y.eq(unlab_outputs1.max(1)[1]).float().sum().item())
            loop_info['u2acc'].append(unlab_y.eq(unlab_outputs2.max(1)[1]).float().sum().item())
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[train][{batch_idx:<3}]", self.gen_info(loop_info, lbs, ubs))
        print(f">>>[train]", self.gen_info(loop_info, label_n, unlab_n, False))
        return loop_info, label_n

    def test_iteration(self, data_loader, print_freq):
        loop_info = defaultdict(list)
        label_n, unlab_n = 0, 0
        for batch_idx, (data, targets) in enumerate(data_loader):
            data, targets = data.to(self.device), targets.to(self.device) 
            lbs, ubs = data.size(0), -1

            ##=== forward ===
            outputs1, outputs2 = self.model(data)
            loss1 = self.ce_loss(outputs1, targets)
            loss2 = self.ce_loss(outputs2, targets)
            loop_info['lSup1'].append(loss1.item())
            loop_info['lSup2'].append(loss2.item())

            with torch.no_grad():
                ema_outputs1,_ = self.ema_model(data)

            ##=== log info ===
            label_n, unlab_n = label_n+lbs, unlab_n+ubs
            loop_info['lacc'].append(targets.eq(outputs1.max(1)[1]).float().sum().item())
            loop_info['l2acc'].append(targets.eq(outputs2.max(1)[1]).float().sum().item())
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[test][{batch_idx:<3}]", self.gen_info(loop_info, lbs, ubs))
        print(f">>>[test]", self.gen_info(loop_info, label_n, unlab_n, False))
        return loop_info, label_n

    def train(self, label_loader, unlab_loader, print_freq=20):
        self.model.train()
        self.ema_model.train()
        with torch.enable_grad():
            return self.train_iteration(label_loader, unlab_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        self.ema_model.eval()
        with torch.no_grad():
            return self.test_iteration(data_loader, print_freq)

    def loop(self, epochs, label_data, unlab_data, test_data, scheduler=None):
        best_acc, n, best_info = 0., 0., None
        for ep in range(epochs):
            self.epoch = ep
            if scheduler is not None: scheduler.step()
            print("------ Training epochs: {} ------".format(ep))
            self.train(label_data, unlab_data, self.print_freq)
            print("------ Testing epochs: {} ------".format(ep))
            info, n = self.test(test_data, self.print_freq)
            acc     = sum(info['lacc']) / n
            if acc>best_acc: best_acc, best_info = acc, info
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep)
        print(f">>>[best]", self.gen_info(best_info, n, n, False))

    def update_ema(self, model, ema_model, alpha, global_step):
        alpha = min(1 - 1 / (global_step +1), alpha)
        for ema_param, param in zip(ema_model.parameters(), model.parameters()):
            ema_param.data.mul_(alpha).add_(1-alpha, param.data)

    def decode_targets(self, targets):
        label_mask = targets.ge(0)
        unlab_mask = targets.le(NO_LABEL)
        targets[unlab_mask] = decode_label(targets[unlab_mask])
        return label_mask, unlab_mask

    def gen_info(self, info, lbs, ubs, iteration=True):
        ret = []
        nums = {'l': lbs, 'u':ubs, 'a': lbs+ubs}
        for k, val in info.items():
            n = nums[k[0]]
            v = val[-1] if iteration else sum(val)
            s = f'{k}: {v/n:.3%}' if k[-1]=='c' else f'{k}: {v:.5f}'
            ret.append(s)
        return '\t'.join(ret)

    def save(self, epoch, **kwargs):
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
