# Codes for DSSDDL, ALC and SDB-Net

(DSSDDL): Discriminative semi-supervised deep dictionary learning for image classification

(ALC): Attention-based label consistency for semi-supervised deep learning

(SDB-Net): Semi-supervised Dual-Branch Network for Image Classification

### main procedure ###

DSSDDL: dssddlv1.py

DSSDDL-MT, ALC and SDB-Net: papers.py

### To run the code ###

```shell
bash run_papers.sh [gpu_id]
```

### The models in trainer/ ###

- trainer/: model files
    - DSSDDLv1.py:  DSSDDL
    - MixDSSDDLv1.py:  DSSDDL + Mixup
    - DSSDDLmtv1.py and DSSDDLmtv2.py: DSSDDL-MeanTeacher (DSSDDL-MT)
    - MixDSSDDLmtv2.py:  DSSDDL-MT + Mixup
    - mtALCv2.py: MeanTeacher+ALC
    - piALCv2.py: PI+ALC
    - SDBNetv2.py: PI+ALC
